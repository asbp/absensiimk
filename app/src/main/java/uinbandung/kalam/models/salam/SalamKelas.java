package uinbandung.kalam.models.salam;

//Model ini hanya untuk ditampilkan dalam menu pilih KRS dan tidak akan masuk ke API.

import android.os.Parcel;
import android.os.Parcelable;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalTime;

public class SalamKelas implements Parcelable {
    public static final Creator<SalamKelas> CREATOR = new Creator<SalamKelas>() {
        @Override
        public SalamKelas createFromParcel(Parcel in) {
            return new SalamKelas(in);
        }

        @Override
        public SalamKelas[] newArray(int size) {
            return new SalamKelas[size];
        }
    };
    DayOfWeek hari;
    LocalTime jam_masuk, jam_selesai;

    int int_hari;

    int h_masuk, m_masuk;
    int h_keluar, m_keluar;
    private String nama_kelas, dosen1, dosen2;

    public SalamKelas(String nama_kelas, String dosen1, String dosen2, DayOfWeek hari, LocalTime jam_masuk, LocalTime jam_selesai) {
        this.nama_kelas = nama_kelas;
        this.dosen1 = dosen1;
        this.dosen2 = dosen2;
        this.hari = hari;
        this.jam_masuk = jam_masuk;
        this.jam_selesai = jam_selesai;

        this.h_masuk = jam_masuk.getHour();
        this.m_masuk = jam_masuk.getMinute();

        this.h_keluar = jam_selesai.getHour();
        this.m_keluar = jam_selesai.getMinute();

        this.int_hari = hari.getValue();
    }

    protected SalamKelas(Parcel in) {
        nama_kelas = in.readString();
        dosen1 = in.readString();
        dosen2 = in.readString();

        int_hari = in.readInt();

        h_masuk = in.readInt();
        m_masuk = in.readInt();
        h_keluar = in.readInt();
        m_keluar = in.readInt();

        hari = DayOfWeek.of(int_hari);
        jam_masuk = LocalTime.of(h_masuk, m_masuk);
        jam_selesai = LocalTime.of(h_keluar, m_keluar);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama_kelas);
        dest.writeString(dosen1);
        dest.writeString(dosen2);

        dest.writeInt(int_hari);

        dest.writeInt(h_masuk);
        dest.writeInt(m_masuk);
        dest.writeInt(h_keluar);
        dest.writeInt(m_keluar);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getNama_kelas() {
        return nama_kelas;
    }

    public void setNama_kelas(String nama_kelas) {
        this.nama_kelas = nama_kelas;
    }

    public String getDosen1() {
        return dosen1;
    }

    public void setDosen1(String dosen1) {
        this.dosen1 = dosen1;
    }

    public String getDosen2() {
        return dosen2;
    }

    public void setDosen2(String dosen2) {
        this.dosen2 = dosen2;
    }

    public DayOfWeek getHari() {
        return hari;
    }

    public void setHari(DayOfWeek hari) {
        this.hari = hari;
    }

    public LocalTime getJam_masuk() {
        return jam_masuk;
    }

    public void setJam_masuk(LocalTime jam_masuk) {
        this.jam_masuk = jam_masuk;
    }

    public LocalTime getJam_selesai() {
        return jam_selesai;
    }

    public void setJam_selesai(LocalTime jam_selesai) {
        this.jam_selesai = jam_selesai;
    }
}
