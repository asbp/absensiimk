package uinbandung.kalam.ui.activities.scanner;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.utils.CommonUtils;
import uinbandung.kalam.utils.Constants;
import uinbandung.kalam.utils.barcode.barcodescanner.zxing.ZXingScannerView;
import uinbandung.kalam.utils.barcode.zxing.core.BarcodeFormat;
import uinbandung.kalam.utils.barcode.zxing.core.Result;

public class QRScanActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    @BindView(R.id.scanview)
    ZXingScannerView mScannerView;
    @BindView(R.id.qrscan_title)
    TextView qrscanTitle;
    @BindView(R.id.menu_qrscan)
    GridLayout menuQrscan;
    @BindView(R.id.camera_flash)
    ToggleButton cameraFlash;
    @BindView(R.id.btn_otp_mahasiswa)
    Button btnOtpMahasiswa;

    List<BarcodeFormat> barcodeFormatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        barcodeFormatList = new ArrayList<>();


        PermissionListener pm = new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                setContentView(R.layout.activity_qrscan);

                ButterKnife.bind(QRScanActivity.this);
                setTitle("Pindai kehadiran");
                setBackButton();

                barcodeFormatList.add(BarcodeFormat.QR_CODE);

                mScannerView.setFormats(barcodeFormatList);
                cameraFlash.setOnClickListener(view -> mScannerView.toggleFlash());



            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                if (response.isPermanentlyDenied()) {
                    CommonUtils.showSettingsDialog(QRScanActivity.this);
                } else {
                    Toast.makeText(QRScanActivity.this, "Izin kamera diperlukan untuk memindai QR Absensi",
                            Toast.LENGTH_SHORT).show();
                }
                finish();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        };

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(pm).check();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mScannerView == null) return;

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView == null) return;

        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        if (mScannerView == null) return;

        String type = rawResult.getBarcodeFormat().toString();

        Log.v(Constants.TAG_QR_SCAN, "Barcode result: " + rawResult.getText()); // Prints scan results
        Log.v(Constants.TAG_QR_SCAN, "Barcode format: " + type); // Prints the scan format (qrcode, pdf417 etc.)

        Bundle bundle = new Bundle();

        bundle.putString("matkul", rawResult.getText());
        bundle.putString("kelas", type);
        bundle.putLong("jadwal", rawResult.getTimestamp());
        bundle.putString("dosen", "Siapa saja");

        launchActivity(QRScanActivity.this, QRScanResultActivity.class, bundle);

        finish();
    }
}
