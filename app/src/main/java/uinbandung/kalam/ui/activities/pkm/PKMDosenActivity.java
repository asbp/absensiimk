package uinbandung.kalam.ui.activities.pkm;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class PKMDosenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pkm_dosen);

        header("Info Program Kreatifitas Mahasiswa");
    }
}
