package uinbandung.kalam.ui.adapters.salam;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.salam.MatkulKRS;
import uinbandung.kalam.models.salam.SalamKelas;
import uinbandung.kalam.utils.DateTimeUtils;

public class PilihKRSAdapter extends RecyclerView.Adapter<PilihKRSAdapter.MyViewHolder> {


    private List<MatkulKRS> matakuliahList;

    private KRSMatkulClick mListener, mCancelListener;

    public PilihKRSAdapter(List<MatkulKRS> matakuliahList, KRSMatkulClick mListener, KRSMatkulClick mCancelListener) {
        this.matakuliahList = matakuliahList;
        this.mListener = mListener;
        this.mCancelListener = mCancelListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_matkul_pilih_krs, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final MatkulKRS item = matakuliahList.get(position);

        holder.pilihKrsNamaMatkul.setText(item.getNama());
        holder.pilihKrsKodeMatkul.setText(item.getKodeMk());

        if (item.getKelasDipilih() > -1) {
            SalamKelas kelas = item.getDaftarKelas().get(item.getKelasDipilih());

            String jadwal = String.format(Locale.US, "%s (%s-%s)",
                    DateTimeUtils.getIndonesianDay(kelas.getHari()),
                    DateTimeUtils.parseTime(kelas.getJam_masuk()),
                    DateTimeUtils.parseTime(kelas.getJam_selesai())
            );

            holder.pilihanKelas.setVisibility(View.VISIBLE);
            holder.txtKelasBelumAda.setVisibility(View.GONE);

            holder.indikatorKelas.setText(kelas.getNama_kelas());
            holder.jadwalKelas.setText(jadwal);
            holder.pilihKrsDosen1.setText(kelas.getDosen1());
            holder.pilihKrsDosen2.setText(kelas.getDosen2());

        } else {
            holder.pilihanKelas.setVisibility(View.GONE);
            holder.txtKelasBelumAda.setVisibility(View.GONE);
        }

        holder.layoutPilihKrs.setOnClickListener(view -> mListener.onClick(holder, item, position));
        holder.krsBatalkanKelas.setOnClickListener(view -> mCancelListener.onClick(holder, item, position));
    }

    @Override
    public int getItemCount() {
        return matakuliahList.size();
    }

    public interface KRSMatkulClick {
        void onClick(MyViewHolder vh, MatkulKRS item, int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_kelas_belum_ada)
        TextView txtKelasBelumAda;
        @BindView(R.id.pilih_krs_nama_matkul)
        TextView pilihKrsNamaMatkul;
        @BindView(R.id.pilih_krs_kode_matkul)
        TextView pilihKrsKodeMatkul;
        @BindView(R.id.indikator_kelas)
        TextView indikatorKelas;
        @BindView(R.id.jadwal_kelas)
        TextView jadwalKelas;
        @BindView(R.id.pilih_krs_dosen_1)
        TextView pilihKrsDosen1;
        @BindView(R.id.pilih_krs_dosen_2)
        TextView pilihKrsDosen2;
        @BindView(R.id.pilihan_kelas)
        RelativeLayout pilihanKelas;
        @BindView(R.id.layout_pilih_krs)
        LinearLayout layoutPilihKrs;
        @BindView(R.id.krs_batalkan_kelas)
        Button krsBatalkanKelas;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
