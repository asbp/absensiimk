package uinbandung.kalam.ui.activities.penelitian.unggah_penelitian;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class UnggahPenelitianActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unggah_penelitian);

        header("Unggah Penelitian");
    }
}
