package uinbandung.kalam.ui.activities.absen;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class AbsenMahasiswaActivity extends BaseActivity {

    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen_mahasiswa);
        ButterKnife.bind(this);

        header("Kehadiran Saya");

        txtNama.setText(getUser().getFullname());
        txtNim.setText(getUser().getUsername());
    }
}
