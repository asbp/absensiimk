package uinbandung.kalam.session;

import org.threeten.bp.LocalDateTime;

public class User {
    private String fullname, username, userlevel;
    private LocalDateTime last_login;

    public User(String fullname, String username, String userlevel, LocalDateTime last_login) {
        this.fullname = fullname;
        this.username = username;
        this.userlevel = userlevel;
        this.last_login = last_login;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(String userlevel) {
        this.userlevel = userlevel;
    }

    public LocalDateTime getLast_login() {
        return last_login;
    }

    public void setLast_login(LocalDateTime last_login) {
        this.last_login = last_login;
    }
}
