package uinbandung.kalam.ui.activities.userlog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseBangetActivity;

public class LupaPasswordActivity extends BaseBangetActivity {

    @BindView(R.id.my_logo)
    ImageView myLogo;
    @BindView(R.id.gmbr_nim)
    ImageView gmbrNim;
    @BindView(R.id.input_ni)
    EditText inputNi;
    @BindView(R.id.gmbr_psw)
    ImageView gmbrPsw;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_ganti_password)
    Button btnGantiPassword;
    @BindView(R.id.kembali_login)
    TextView kembaliLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);
        ButterKnife.bind(this);

        btnGantiPassword.setOnClickListener(view -> launchActivity(LupaPasswordActivity.this, KembaliLoginActivity.class));

        Glide.with(this)
                .load(R.drawable.logo)
                .apply(new RequestOptions().override(300))
                .into(myLogo);
    }


    public void kembalilogin(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.kembali_login: // R.id.textView1
                intent = new Intent(this, LoginActivity.class);
                break;
            default:
                intent = new Intent();
                break;
        }
        startActivity(intent);
    }


}
