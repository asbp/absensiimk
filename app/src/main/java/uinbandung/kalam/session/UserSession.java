package uinbandung.kalam.session;

import android.content.Context;

import com.securepreferences.SecurePreferences;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

public class UserSession {
    private static UserSession instance; //Instance itself

    private static Context appContext;
    private SecurePreferences prefs;
    private User currentUser;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private UserSession() {
        prefs = new SecurePreferences(appContext, "app", "userlog.xml");
        SecurePreferences.setLoggingEnabled(true);
    }

    public static void init(Context context) {
        if (appContext == null) {
            appContext = context;
        }
    }

    public static Context get() {
        return getInstance().getContext();
    }

    public static UserSession getInstance() {
        return instance == null ?
                (instance = new UserSession()) :
                instance;
    }

    private Context getContext() {
        return appContext;
    }

    //-----------------------------------

    public User getCurrentUser() {
        if (!prefs.contains("logged")) return null;

        if (prefs.getBoolean("logged", true) && this.currentUser == null) {
            LocalDateTime dt = LocalDateTime.parse(prefs.getString("last_login", ""), formatter);
            this.currentUser = new User(prefs.getString("fullname", ""), prefs.getString("username", ""), prefs.getString("user_level", ""), dt);
        }
        return currentUser;
    }

    public void setCurrentUser(String fullname, String username, String user_level) {
        SecurePreferences.Editor userEditor = prefs.edit();
        LocalDateTime last_login = LocalDateTime.now();

        this.currentUser = new User(fullname, username, user_level, last_login);
        userEditor.putBoolean("logged", true);
        userEditor.putString("fullname", fullname);
        userEditor.putString("username", username);
        userEditor.putString("user_level", user_level);
        userEditor.putString("last_login", formatter.format(last_login));

        userEditor.apply();
    }

    public void logout() {
        SecurePreferences.Editor userEditor = prefs.edit();
        userEditor.remove("logged");
        userEditor.remove("fullname");
        userEditor.remove("username");
        userEditor.remove("user_level");
        userEditor.remove("last_login");

        userEditor.apply();
    }
}
