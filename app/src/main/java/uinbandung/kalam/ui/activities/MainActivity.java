package uinbandung.kalam.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.session.User;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.activities.scanner.QRScanActivity;
import uinbandung.kalam.ui.fragments.HomeFragment;
import uinbandung.kalam.ui.fragments.presensi.PresensiDosenFragment;
import uinbandung.kalam.ui.fragments.presensi.PresensiFragment;
import uinbandung.kalam.ui.fragments.ProfileFragment;
import uinbandung.kalam.ui.fragments.jadwal.JadwalMahasiswaFragment;

public class MainActivity extends BaseActivity {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;

    boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.space)
    SpaceNavigationView spaceNavigationView;
    @BindView(R.id.fab_scan)
    FloatingActionButton fabScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        User u = getUser();

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // load the store fragment by default
        loadFragment(R.id.frame_container, new HomeFragment());

        if(u.getUserlevel().equalsIgnoreCase(Access.DOSEN)) {
            spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
            spaceNavigationView.setVisibility(View.GONE);
            fabScan.setOnClickListener(view -> launchActivity(MainActivity.this, QRScanActivity.class));
            //fabScan.setVisibility(View.VISIBLE);
        } else if(u.getUserlevel().equalsIgnoreCase(Access.MAHASISWA)) {
            spaceNavigationView.initWithSaveInstanceState(savedInstanceState);

            spaceNavigationView.addSpaceItem(new SpaceItem("HOME", R.drawable.ic_home_black_24dp));
            //spaceNavigationView.addSpaceItem(new SpaceItem("KEHADIRAN", R.drawable.ic_assignment_black_24dp));
            //spaceNavigationView.addSpaceItem(new SpaceItem("JADWAL", R.drawable.ic_schedule_black_24dp));
            spaceNavigationView.addSpaceItem(new SpaceItem("PROFIL", R.drawable.ic_person_black_24dp));
            spaceNavigationView.showIconOnly();

            spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
                @Override
                public void onCentreButtonClick() {
                    launchActivity(MainActivity.this, QRScanActivity.class);
                }

                @Override
                public void onItemClick(int itemIndex, String itemName) {
                    Fragment fragment;
                    switch (itemIndex) {
                        case 0:
                            fragment = new HomeFragment();
                            loadFragment(R.id.frame_container, fragment);
                            break;
                        case 1:
                            fragment = new ProfileFragment();
                            loadFragment(R.id.frame_container, fragment);
                            break;
                    }

                    // Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onItemReselected(int itemIndex, String itemName) {
                    //   Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(MainActivity.this, R.string.back_again, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

}
