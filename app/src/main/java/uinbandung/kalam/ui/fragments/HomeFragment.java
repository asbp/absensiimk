package uinbandung.kalam.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.models.menus.MainMenu;
import uinbandung.kalam.models.news.NewsHome;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.MoreMenuActivity;
import uinbandung.kalam.ui.activities.absen.AbsenMahasiswaActivity;
import uinbandung.kalam.ui.activities.event.EventActivity;
import uinbandung.kalam.ui.activities.jadwal.JadwalActivity;
import uinbandung.kalam.ui.activities.jadwal.JadwalDosenActivity;
import uinbandung.kalam.ui.activities.penelitian.DosenPenelitianActivity;
import uinbandung.kalam.ui.activities.pkm.PKMDosenActivity;
import uinbandung.kalam.ui.activities.salam.matakuliah.DaftarMatkulActivity;
import uinbandung.kalam.ui.adapters.menus.MainMenuAdapter;
import uinbandung.kalam.ui.adapters.news.NewsHomeAdapter;
import uinbandung.kalam.ui.layoutmanager.HomeMenuLayoutManager;
import uinbandung.kalam.ui.layoutmanager.HomeNewsLayoutManager;
import uinbandung.kalam.utils.CommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    @BindView(R.id.background)
    ImageView background;
    @BindView(R.id.malam)
    CardView malam;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.white_card)
    CardView whiteCard;
    @BindView(R.id.frame)
    RelativeLayout frame;
    Unbinder unbinder;
    @BindView(R.id.textGreeting)
    TextView txtGreeting;
    @BindView(R.id.main_menu_rv)
    RecyclerView mainMenuRv;
    @BindView(R.id.home_user_fullname)
    TextView homeUserFullname;
    @BindView(R.id.btn_semua_berita)
    Button btnSemuaBerita;
    @BindView(R.id.rv_news_top)
    RecyclerView rvNewsTop;

    List<MainMenu> menus;
    List<NewsHome> news;
    MainMenuAdapter mainMenuAdapter;
    NewsHomeAdapter newsHomeAdapter;

    User u;
    @BindView(R.id.news_card_home)
    LinearLayout newsCardHome;

    public HomeFragment() {
        // Required empty public constructor
        menus = new ArrayList<>();
        mainMenuAdapter = new MainMenuAdapter(menus);
        u = UserSession.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        mainMenuRv.setLayoutManager(new HomeMenuLayoutManager(view.getContext(), 4));
        mainMenuRv.setAdapter(mainMenuAdapter);

        btnSemuaBerita.setOnClickListener(view1 -> {
            launchActivity(view.getContext(), EventActivity.class);
        });

        initHome();
        newsCardHome.setVisibility(View.GONE);
        initMenus(view);
        //initNews();

        return view;
    }

    private void initHome() {
        Glide.with(this)
                .load(CommonUtils.getDayResource())
                .into(background);

        setGreeting();
    }

    private void initNews() {
        news = new ArrayList<>();
        newsHomeAdapter = new NewsHomeAdapter(news);
        RecyclerView.LayoutManager mLayoutManager = new HomeNewsLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvNewsTop.setLayoutManager(mLayoutManager);
        rvNewsTop.setAdapter(newsHomeAdapter);
        loadNews();
    }

    private void loadNews() {
        news.add(new NewsHome("1f", "Fakultas Sains dan Teknologi", "Lorem ipsum...", "https://rencanamu.id/assets/file_uploaded/blog/1567519707-science-an.jpg"));
        news.add(new NewsHome("1f", "Teknik Informatika", "Fokus", "https://kampusaja.com/wp-content/uploads/2016/01/teknik-informatika_v2.jpg"));
    }

    private void initMenus(View view) {
        switch (u.getUserlevel()) {
            case Access.MAHASISWA:
                menus.add(new MainMenu("Kehadiran saya", R.drawable.ic_basic_todo_txt, v -> launchActivity(view.getContext(), AbsenMahasiswaActivity.class)));
                menus.add(new MainMenu("Jadwal saya", R.drawable.ic_carbon_list, v -> launchActivity(view.getContext(), JadwalActivity.class)));
                break;

            case Access.DOSEN:
                menus.add(new MainMenu("Matakuliah", R.drawable.ic_basic_todo_txt, v -> launchActivity(view.getContext(), JadwalDosenActivity.class)));
                break;
        }

        menus.add(new MainMenu("Lebih Banyak", R.drawable.ic_carbon_grid, v -> launchActivity(view.getContext(), MoreMenuActivity.class)));
//219653
    }

    private void setGreeting() {
        String greeting = "";

        switch (CommonUtils.getDayType()) {
            case CommonUtils.DAY_MORNING:
                greeting = "Selamat pagi, ";
                break;
            case CommonUtils.DAY_MIDDAY:
                greeting = "Selamat siang, ";
                break;
            case CommonUtils.DAY_AFTERNOON:
                greeting = "Selamat sore, ";
                break;
            case CommonUtils.DAY_NIGHT:
                greeting = "Selamat malam, ";
                break;
        }

        txtGreeting.setText(String.format(Locale.US, "%s", greeting));

        homeUserFullname.setText(u.getFullname());
    }

}
