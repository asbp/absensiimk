package uinbandung.kalam.models.jadwal;

public class Matakuliah {
    private String kodeMk, jadwal, nama;

    public Matakuliah(String kodeMk, String jadwal, String nama) {
        this.kodeMk = kodeMk;
        this.jadwal = jadwal;
        this.nama = nama;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
