package uinbandung.kalam.ui.activities.scanner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class OtpMahasiswaActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_mahasiswa);

        header("OTP Code");
    }
}
