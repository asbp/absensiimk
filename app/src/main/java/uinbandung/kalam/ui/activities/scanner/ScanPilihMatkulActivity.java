package uinbandung.kalam.ui.activities.scanner;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class ScanPilihMatkulActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_pilih_matkul);

        header("Pilih Matakuliah Dosen");
    }
}
