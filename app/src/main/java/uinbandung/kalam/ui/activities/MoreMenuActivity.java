package uinbandung.kalam.ui.activities;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.shreyaspatil.MaterialDialog.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.models.menus.MoreMenu;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.activities.info.InfoDosenActivity;
import uinbandung.kalam.ui.activities.info.InfoPimpinanActivity;
import uinbandung.kalam.ui.activities.jadwal.JadwalActivity;
import uinbandung.kalam.ui.adapters.SimpleSectionedRecyclerViewAdapter;
import uinbandung.kalam.ui.adapters.menus.MoreMenuAdapter;

public class MoreMenuActivity extends BaseActivity {

    @BindView(R.id.more_menu_rv)
    RecyclerView moreMenuRv;

    List<MoreMenu> moreMenuList;
    List<SimpleSectionedRecyclerViewAdapter.Section> sections;
    MoreMenuAdapter menuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_menu);
        ButterKnife.bind(this);

        setBackButton();
        setTitle("Menu Lebih Lengkap");

        moreMenuList = new ArrayList<>();
        sections = new ArrayList<>();

        moreMenuRv.setLayoutManager(new LinearLayoutManager(MoreMenuActivity.this));
        moreMenuRv.setItemAnimator(new DefaultItemAnimator());
        menuAdapter = new MoreMenuAdapter(moreMenuList);

        setMoreMenuList();

        //Add your adapter to the sectionAdapter
        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(this, R.layout.item_section, R.id.section_text, menuAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));

        //Apply this adapter to the RecyclerView
        moreMenuRv.setAdapter(mSectionedAdapter);
    }

    private void addSection(String text) {
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(menuAdapter.getItemCount(), text));
    }

    private void setMoreMenuList() {

        addSection("Akun");
        moreMenuList.add(new MoreMenu("Logout", R.drawable.ic_carbon_logout, v -> {
            new MaterialDialog.Builder(MoreMenuActivity.this)
                    .setCancelable(false)
                    .setTitle("Anda yakin?")
                    .setAnimation(R.raw.logout_anim)
                    .setMessage("Anda akan keluar dari aplikasi ini.")
                    .setPositiveButton("Ya", (dialogInterface, which) -> {
                        logout(MoreMenuActivity.this);
                    })
                    .setNegativeButton("Tidak", (dialogInterface, which) -> dialogInterface.dismiss())
                    .build().show();
            //Show message
        }));

        addSection("Lebih Lanjut");
        moreMenuList.add(new MoreMenu("Tentang Aplikasi Ini", R.drawable.ic_carbon_information, v -> launchActivity(MoreMenuActivity.this, AboutActivity.class)));
    }
}
