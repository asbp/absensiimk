package uinbandung.kalam.ui.activities.jadwal;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.fragments.jadwal.JadwalMahasiswaFragment;

public class JadwalActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal);

        header("Jadwal Saya");

        loadFragment(R.id.jadwal_frame, new JadwalMahasiswaFragment());
    }
}
