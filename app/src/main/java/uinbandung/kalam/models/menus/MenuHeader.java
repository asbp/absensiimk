package uinbandung.kalam.models.menus;

public class MenuHeader {
    private String caption;

    public MenuHeader(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
