package uinbandung.kalam.ui.activities.scanner.metode_absen_dosen.check_list_mahasiswa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class CheckListMahasiswaActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list_mahasiswa);

        header("Check List Kelas");
    }
}
