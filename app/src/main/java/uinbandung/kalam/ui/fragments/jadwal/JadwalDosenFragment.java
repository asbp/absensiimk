package uinbandung.kalam.ui.fragments.jadwal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.fragments.BaseFragment;

public class JadwalDosenFragment extends BaseFragment {


    public JadwalDosenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jadwal_dosen, container, false);
    }

}
