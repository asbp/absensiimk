package uinbandung.kalam.ui.activities.salam.kartu;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.salam.SalamKelas;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.adapters.salam.PilihKelasAdapter;

public class PilihKelasKRSActivity extends BaseActivity {

    @BindView(R.id.rv_pilih_kelas_krs_mhs)
    RecyclerView rvPilihKelasKrsMhs;

    PilihKelasAdapter mAdapter;
    PilihKelasAdapter.KelasClick mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kelas_krs);
        ButterKnife.bind(this);

        header("Pilih Kelas");

        Bundle b = getIntent().getExtras();

        int kelas = b.getInt("kelas");
        ArrayList<SalamKelas> salamKelasArrayList = getIntent().getParcelableArrayListExtra("items");

        mListener = (vh, item, pos) -> {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("kelas", kelas);
            returnIntent.putExtra("pos", pos);
            setResult(BaseActivity.RESULT_OK, returnIntent);
            finish();
        };

        mAdapter = new PilihKelasAdapter(salamKelasArrayList, mListener);

        rvPilihKelasKrsMhs.setLayoutManager(new LinearLayoutManager(this));
        rvPilihKelasKrsMhs.setAdapter(mAdapter);

    }
}
