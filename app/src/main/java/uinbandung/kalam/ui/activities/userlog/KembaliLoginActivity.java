package uinbandung.kalam.ui.activities.userlog;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseBangetActivity;

public class KembaliLoginActivity extends BaseBangetActivity {

    @BindView(R.id.my_logo)
    ImageView myLogo;
    @BindView(R.id.btn_kembali)
    Button btnKembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kembali_login);
        ButterKnife.bind(this);

        btnKembali.setOnClickListener(view -> launchActivity(KembaliLoginActivity.this, LoginActivity.class));

        Glide.with(this)
                .load(R.drawable.logo)
                .apply(new RequestOptions().override(300))
                .into(myLogo);
    }


}
