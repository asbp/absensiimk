package uinbandung.kalam.ui.fragments.salam;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.salam.kartu.KHSActivity;
import uinbandung.kalam.ui.activities.salam.kartu.KRSActivity;
import uinbandung.kalam.ui.activities.salam.matakuliah.DaftarMatkulActivity;
import uinbandung.kalam.ui.fragments.BaseFragment;
import uinbandung.kalam.utils.CommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalamMahasiswaFragment extends BaseFragment {


    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.daftar_matkul)
    Button daftarMatkul;
    @BindView(R.id.btn_krs)
    Button btnKrs;
    @BindView(R.id.btn_khs)
    Button btnKhs;

    private Context c;

    public SalamMahasiswaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_salam_mahasiswa, container, false);
        bindView(this, v);

        c = v.getContext();

        User user = UserSession.getInstance().getCurrentUser();

        String input = user.getUserlevel();

        txtNama.setText(user.getFullname());
        txtNim.setText(user.getUsername());
        txtStatus.setText(CommonUtils.capitalizeFirstLetter(input));

        daftarMatkul.setOnClickListener(view -> launchActivity(c, DaftarMatkulActivity.class));
        btnKrs.setOnClickListener(view -> launchActivity(c, KRSActivity.class));
        btnKhs.setOnClickListener(view -> launchActivity(c, KHSActivity.class));

        return v;
    }

}
