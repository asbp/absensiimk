package uinbandung.kalam;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

import uinbandung.kalam.session.UserSession;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        UserSession.init(this);
    }
}
