package uinbandung.kalam.ui.activities.salam.matakuliah;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class PrasyaratMatkulActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prasyarat_matkul);

        header("Prasyarat Matakuliah");
    }
}
