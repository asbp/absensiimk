package uinbandung.kalam.ui.fragments.salam;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenBimbinganAkademikActivity;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenIbadahActivity;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenKerjaPraktikActivity;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenTahfidzActivity;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenTilawahActivity;
import uinbandung.kalam.ui.activities.salam.salamDosen.DosenTugasAkhirActivity;
import uinbandung.kalam.ui.fragments.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalamDosenFragment extends BaseFragment {


    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.btn_bimbingan_akademik)
    Button btnBimbinganAkademik;
    @BindView(R.id.btn_ibadah)
    Button btnIbadah;
    @BindView(R.id.btn_tilawah)
    Button btnTilawah;
    @BindView(R.id.btn_tahfidz)
    Button btnTahfidz;
    @BindView(R.id.btn_kerja_praktik)
    Button btnKerjaPraktik;
    @BindView(R.id.btn_tugas_akhir)
    Button btnTugasAkhir;

    private Context c;

    public SalamDosenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_salam_dosen, container, false);
        bindView(this, v);

        c = v.getContext();

        btnBimbinganAkademik.setOnClickListener(view -> launchActivity(c, DosenBimbinganAkademikActivity.class));
        btnIbadah.setOnClickListener(view -> launchActivity(c, DosenIbadahActivity.class));
        btnTilawah.setOnClickListener(view -> launchActivity(c, DosenTilawahActivity.class));
        btnTahfidz.setOnClickListener(view -> launchActivity(c, DosenTahfidzActivity.class));
        btnKerjaPraktik.setOnClickListener(view -> launchActivity(c, DosenKerjaPraktikActivity.class));
        btnTugasAkhir.setOnClickListener(view -> launchActivity(c, DosenTugasAkhirActivity.class));

        return v;
    }

}
