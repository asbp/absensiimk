package uinbandung.kalam.models.menus;

import android.view.View;

public class MoreMenu extends MainMenu {
    public MoreMenu(String name, int icon, View.OnClickListener onClick) {
        super(name, icon, onClick);
    }
}
