package uinbandung.kalam.ui.adapters.menus;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.menus.MoreMenu;

public class MoreMenuAdapter extends RecyclerView.Adapter<MoreMenuAdapter.MyViewHolder> implements Filterable {

    List<MoreMenu> moreMenuList;
    List<MoreMenu> copyList;


    public MoreMenuAdapter(List<MoreMenu> moreMenuList) {
        this.moreMenuList = moreMenuList;
        this.copyList = moreMenuList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            MoreMenu item = moreMenuList.get(position);

            holder.moreMenuText.setText(item.getName());

            Glide.with(holder.itemView.getContext())
                    .load(item.getIcon())
                    .into(holder.moreMenuIcon);

            if (item.getOnClick() != null)
                holder.moreMenuLayout.setOnClickListener(item.getOnClick());
        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public int getItemCount() {
        return moreMenuList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();

                if (charString.isEmpty()) moreMenuList = copyList;
                else {
                    List<MoreMenu> filteredList = new ArrayList<>();
                    filteredList.clear();

                    for (MoreMenu row : copyList) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    moreMenuList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = moreMenuList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                moreMenuList = (ArrayList<MoreMenu>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.more_menu_icon)
        ImageView moreMenuIcon;
        @BindView(R.id.more_menu_text)
        TextView moreMenuText;
        @BindView(R.id.more_menu_layout)
        RelativeLayout moreMenuLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
