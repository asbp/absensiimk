package uinbandung.kalam.ui.activities.penelitian;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.activities.penelitian.cari_penelitian.DosenCariPenelitianActivity;
import uinbandung.kalam.ui.activities.penelitian.unggah_penelitian.UnggahPenelitianActivity;

public class DosenPenelitianActivity extends BaseActivity {

    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.cariPenelitian)
    Button cariPenelitian;
    @BindView(R.id.unggahPenelitian)
    Button unggahPenelitian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_penelitian);
        ButterKnife.bind(this);

        header("Penelitian");

        cariPenelitian.setOnClickListener(v -> {
            launchActivity(DosenPenelitianActivity.this, DosenCariPenelitianActivity.class);
        });

        unggahPenelitian.setOnClickListener(v -> {
            launchActivity(DosenPenelitianActivity.this, UnggahPenelitianActivity.class);
        });

    }


}
