package uinbandung.kalam.ui.fragments.presensi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;


public class PresensiDosenFragment extends Fragment {


    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.spinnerBulanPresensi)
    Spinner spinnerBulanPresensi;

    public PresensiDosenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_presensi_dosen, container, false);

        String [] values =
                {"Agustus","September","Oktober","November","Desember",};
        Spinner spinner = (Spinner) v.findViewById(R.id.spinnerBulanPresensi);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        spinner.setAdapter(adapter);

        return v;
    }

}
